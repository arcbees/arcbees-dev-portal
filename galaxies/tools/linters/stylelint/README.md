# @arcbees/stylelint

Arcbees’s stylelint rules and configs.

## Installation

Install `@arcbees/stylelint`, [stylelint](https://github.com/stylelint/stylelint/), [stylelint-order](https://github.com/hudochenkov/stylelint-order/).

```bash
yarn add --dev @arcbees/stylelint stylelint stylelint-order
```

## Usage
Arcbees's preferred rules come bundled in this package. To use them in your project, create a `.stylelintrc` file at the root of your project and extend `@arcbees/stylelint`.
```json
{
  "extends": "@arcbees/stylelint"
}
```

You can also create a script in your `package.json` and run it with yarn `yarn stylelint`.
```json
{
  "scripts": {
    "stylelint": "stylelint \"./src/**/*.scss\""
  }
}
```

## Configuration

Some of the rules provided by this package may not suit the needs of your project. You can override them in `.stylelintrc`:

```json
{
  "extends": "@arcbees/stylelint",
  "rules": {
    "selector-type-no-unknown": null
  }
}
```
