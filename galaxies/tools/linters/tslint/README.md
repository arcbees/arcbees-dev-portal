# @arcbees/tslint

Arcbees’s TSlint rules and configs.

## Installation

Install `@arcbees/tslint`, [tslint](https://palantir.github.io/tslint/), [codelyzer](https://github.com/mgechev/codelyzer/) and [tslint-eslint-rules](https://github.com/buzinas/tslint-eslint-rules/).

```bash
yarn add --dev @arcbees/tslint tslint codelyzer tslint-eslint-rules
```

## Usage
Arcbees's preferred rules come bundled in this package. To use them in your project, create a `tslint.json` file at the root of your project and extend `@arcbees/tslint`.
```json
{
  "extends": "@arcbees/tslint"
}
```

You can also create a script in your `package.json` and run it with yarn `yarn tslint`.
```json
{
  "scripts": {
    "tslint": "tslint \"./src/**/*.{ts,tsx}\" -p tsconfig.json"
  }
}
```

## Configuration

Some of the rules provided by this package may not suit the needs of your project. You can override them in `tslint.json`:

```json
{
  "extends": "@arcbees/tslint",
  "rules": {
    "component-selector": [ true, "element", "my-prefix", "kebab-case" ],
    "directive-selector": [ true, "attribute", "myPrefix", "camelCase" ],
    "pipe-naming": [ true, "camelCase", "myPrefix" ]
  }
}
```
