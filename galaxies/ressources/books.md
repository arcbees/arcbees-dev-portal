# Books to read

* [Clean Code](https://www.amazon.ca/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882) - The bible for clean coding, a must-read for every developers.

--
_This section is a part of the Ressources Galaxy. You can wrap to any galaxy inside the [Arcbees Dev Portal](../../README.md)_.