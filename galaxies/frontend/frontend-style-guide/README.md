# Frontend Style Guide

This Frontend Style Guide contains best practices and basic frontend rules to apply when working on an Arcbees' project. Rules are created to ensure cohesion in the code produced, to ease maintainability and give a common and known structure to work on.

We aim at producing something that will be able to grow and scale, while keeping it human readable and understandable for a newcomer. A balance between speed, ease and simplicity.

## Chapters

### [HTML - Structure and Rules](html-rules.md)

### [CSS - Structure and Rules](css-rules.md)

--
_This section is a part of the Frontend Galaxy. You can wrap to any galaxy inside the [Arcbees Dev Portal](../../../README.md)_.