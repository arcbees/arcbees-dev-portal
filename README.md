<div align="center">
  <h1>Arcbees Dev Portal of everything</h1>

  _Best practices, linters, styleguides and anything related to web and app dev._

  ![Arcbees Dev Book of everything](assets/portal.png)
</div> 

## Galaxies

### The Frontend Galaxy

#### - [Frontend Style Guide](galaxies/frontend/frontend-style-guide/README.md)

This Frontend Style Guide contains best practices and basic frontend rules to apply when working on an Arcbees' project. Rules are created to ensure cohesion in the code produced, to ease maintainability and give a common and known structure to work on.

#### - [SCSS Boilerplate](galaxies/frontend/scss-boilerplate/README.md)

The default SCSS folders and files for common websites.  

### The Backend Galaxy _(coming soon)_

### The Tooling Galaxy

#### - [Linters](tools/linters)

### The Ressources Galaxy

#### - [Books to read](galaxies/ressources/books.md)

## About Arcbees
Coming from the Open Source community, we always try to share our knowledge as much as we can. We truly believe that being open about our practices and the way we work is the only way to go forward and improve. Feel free to comment and ask questions!

[Visit our website](https://www.arcbees.com) and [follow us](http://www.twitter.com/arcbees).
